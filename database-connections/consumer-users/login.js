/* global request, WrongUsernameOrPasswordError */
// eslint-disable-next-line no-unused-vars
function login(email, password, cb) {
  // Logger helper
  const opts = {
    endpoint:
      "https://collectors.de.sumologic.com/receiver/v1/http/ZaVnC4dhaV3sU4phFS1JaYyNyaKvDbFVxAq5nHyeGLw_HYETMlxhWEPxPqRtZrE9dzPQq1Kbm7VyB2r-2IphHPHSqtdpR90m44YnAKs1qPoRtcobQAfDkQ==",
    sessionKey: "476a0232-0d07-11eb-862d-a32b4e127252",
    hostname: "gslab.accounts.travel0.net",
    sourceCategory: "demo/tenant",
    sourceName: "gslab-partner",
    // ... any other options ...
    onError: () => {
      // eslint-disable-next-line no-use-before-define
      console.log("ERROR", "-", "Sending message to SumoLogic Failed");
    },
  };

  const SumoLogger = require("sumo-logger");
  // Instantiate the SumoLogger
  const sumoLogger = new SumoLogger(opts);

  // Write to log
  function doLog(level, msg) {
    const logLevel = level.toUpperCase();
    sumoLogger.log({
      logLevel,
      demoName: "gslab",
      tenant: "gslab-partner",
      action: "consumer_custom_db_login_script_execution",
      hostname: "gslab.accounts.travel0.net",
      // Do we need this with all?
      description: msg,
    });
  }
  doLog("INFO", "Starting Consumer Login User Db Script");

  function isEmail(data) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(data).toLowerCase());
  }

  const payload = { password };

  if (isEmail(email)) {
    payload.email = email;
  } else {
    payload.username = email;
  }

  // Connect to the existing Travel application authentication API to lazily migration users
  request(
    {
      url: "https://gslab.travel0.net/api/v1/auth",
      method: "POST",
      json: payload,
    },
    (error, response, body) => {
      if (error) {
        doLog("ERROR", `Failed to login ${error.toString()}`);
        cb(error);
      } else if (body.error === "invalid username or password") {
        doLog("ERROR", `Failed to login ${body.error}`);
        cb(new WrongUsernameOrPasswordError(email));
      } else if (body.error) {
        doLog("ERROR", `Failed to login ${body.error}`);
        cb(body.error);
      } else if (response.statusCode !== 200) {
        doLog(
          "ERROR",
          `Failed to login unknown error status: ${response.statusCode}`
        );
        cb(new WrongUsernameOrPasswordError(email));
      } else {
        const user = body.profile;

        const profile = {
          // Ensure the ID is unique per deploy/build
          id: Buffer.from(`${user.email}${user.id}gslab-78`)
            .toString("base64")
            .replace(/\+/g, "-")
            .replace(/\//g, "_")
            .replace(/=+$/, ""),
          email: user.email,
          email_verified: user.email_verified,
          given_name: user.first_name,
          family_name: user.last_name,
          nickname: user.display_name,
          username: user.username,
          name: user.display_name,
          app_metadata: {
            migrated: true,
          },
        };

        // If API returns this email has duplicates then set the email to a temp email
        // which will be picked up by the de-dupe rule/app.
        if (user.isDuplicateEmail) {
          profile.email = `${profile.username}@duplicate.email`;
          profile.email_verified = false;
          profile.app_metadata.duplicate_email = user.email;
        }

        doLog("INFO", `Credentials Found for user ${JSON.stringify(profile)}`);
        cb(null, profile);
      }
    }
  );
}
